#include "controller/controller.h"
#include "packet/packet.h"

namespace weilin {
	Controller::Controller(const uint32_t& seq, const packet_shared_ptr& send_packet_sp) :
		m_send_packet_sp(send_packet_sp)
	{
		m_send_packet_sp->SetSeq(seq);
	}
	Controller::~Controller()
	{

	}
	const packet_shared_ptr& Controller::GetSendPacket()
	{
		return m_send_packet_sp;
	}
}
