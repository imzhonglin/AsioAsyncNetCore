#include "callback/callback.h"
#include "packet/packet.h"

namespace weilin
{
	CallBack::CallBack(const SendRecvResultCb& sendRecvResultCb): 
	m_SendRecvResultCb(sendRecvResultCb), 
	m_SendResultCb(NULL),
	m_RecvPacketCb(NULL),
	m_AcceptedHandleCb(NULL), 
	m_SocketStateHandleCb(NULL)
	{

	}

	CallBack::CallBack(const SendResultCb& sendResultCb, const RecvPacketCb& recvPacketCb) :
	m_SendRecvResultCb(NULL),
	m_SendResultCb(sendResultCb),
	m_RecvPacketCb(recvPacketCb),
	m_AcceptedHandleCb(NULL),
	m_SocketStateHandleCb(NULL)
	{

	}
	CallBack::~CallBack()
	{
		m_SendRecvResultCb = NULL;
		m_RecvPacketCb = NULL;
		m_SocketStateHandleCb = NULL;
		m_AcceptedHandleCb = NULL;
	}
	void CallBack::SendRecvResult(const controller_shared_ptr& controller_sp, const SendRecvStatus& send_recv_status)
	{
		if (m_SendRecvResultCb == NULL)
		{
			return;
		}
		m_SendRecvResultCb(controller_sp, send_recv_status);
	}
	void CallBack::SendResult(const packet_shared_ptr& send_packet_sp, const SendStatus& send_status)
	{
		if (m_SendResultCb == NULL)
		{
			return;
		}
		m_SendResultCb(send_packet_sp, send_status);
	}
	void CallBack::RecvPacket(const channel_core_weak_ptr& channel_core_wp, const packet_shared_ptr& request_packet_sp)
	{
		if (m_RecvPacketCb == NULL)
		{
			return;
		}
		m_RecvPacketCb(channel_core_wp, request_packet_sp);
	}
	void CallBack::SocketStateHandle(const channel_core_shared_ptr& channel_core_sp, const SocketStatus& socket_status)
	{
		if (m_SocketStateHandleCb == NULL)
		{
			return;
		}
		m_SocketStateHandleCb(channel_core_sp, socket_status);
	}

	void CallBack::AcceptedHandle(const socket_shared_ptr& socket_sp)
	{
		if (m_AcceptedHandleCb == NULL)
		{
			return;
		}
		m_AcceptedHandleCb(socket_sp);
	}

	void CallBack::SetSocketStateHandleCb(const SocketStateHandleCb& socketStateHandleCb)
	{
		m_SocketStateHandleCb = socketStateHandleCb;
	}

	void CallBack::SetAcceptedHandleCb(const AcceptedHandleCb& acceptedHandleCb)
	{
		m_AcceptedHandleCb = acceptedHandleCb;
	}
}
