#include "packet/packet.h"
#include "packet/http_response_packet.h"
#include "base/util_string.h"

//cmd packet
static const char const_http_enter_char = '\r';
static const char const_http_newline_char = '\n';
static const char  const_http_colon_char = ':';
static const char  const_http_space_char = ' ';
static const char const_http_enter_newline_str[] = { '\r', '\n' };
static const char* const const_http_type_key = "Content-Type";
static const char* const const_http_type_value = "application/json";
static const char* const const_http_length_key = "Content-Length";
static const char* const const_http_top_str = "HTTP/1.1 200 OK";

namespace weilin {

	HttpResponsePacket::HttpResponsePacket()
	{
		m_head.Reset();
	}
	HttpResponsePacket::~HttpResponsePacket()
	{
	}

	bool HttpResponsePacket::Tobuffer(string& outBuffStr)
	{
		outBuffStr.clear();
		outBuffStr.append(const_http_top_str);


		if (m_head_map.find(const_http_type_key) != m_head_map.end())
		{
			return false;
		}
		m_head_map[const_http_type_key] = const_http_type_value;

		if (m_head_map.find(const_http_length_key) != m_head_map.end())
		{
			return false;
		}
		m_head_map[const_http_length_key] = boost::lexical_cast<string>(m_body.length());;

		outBuffStr.append(const_http_enter_newline_str);

		for (map<string, string>::iterator index_it = m_head_map.begin(); index_it != m_head_map.end(); index_it++)
		{
			outBuffStr.append(index_it->first);
			outBuffStr.push_back(const_http_colon_char);
			outBuffStr.push_back(const_http_space_char);
			outBuffStr.append(index_it->second);
			outBuffStr.append(const_http_enter_newline_str);
		}
		outBuffStr.append(const_http_enter_newline_str);
		outBuffStr.append(m_body);
		return true;
	}

	ConsumerRetStatus HttpResponsePacket::Consume(const char& input)
	{
		return CONSUM_RET_FAIL;
	}
}
