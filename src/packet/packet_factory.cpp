#include "comm/comm_def.h"
#include "packet/packet_factory.h"
#include "packet/packet.h"
#include "packet/cmd_packet.h"
#include "packet/http_request_packet.h"
#include "packet/http_response_packet.h"

namespace weilin
{
	packet_shared_ptr PacketFactory::GetPackect(const ChannelType& channel_type, const PacketType& packet_type)
	{
		packet_shared_ptr packet_sp;
		if (channel_type == CHANNEL_CMD)
		{
			cmd_packet_shared_ptr cmd_packet_sp = boost::make_shared<CmdPacket>();
			packet_sp = cmd_packet_sp;
		}
		else if (channel_type == CHANNEL_HTTP)
		{
			if (packet_type == PACKET_REQUEST)
			{
				http_request_packet_shared_ptr http_request_packet_sp = boost::make_shared<HttpRequestPacket>();
				packet_sp = http_request_packet_sp;
			}
			else
			{
				http_response_packet_shared_ptr http_response_packet_sp = boost::make_shared<HttpResponsePacket>();
				packet_sp = http_response_packet_sp;
			}
		}
		return packet_sp;
	}
}
