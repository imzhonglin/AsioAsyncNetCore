#include "packet/packet.h"
#include "log/log.h"
static const char* const_cmd_seq_key = "Seq";

namespace weilin {
	Packet::Packet():
	m_is_request(false),
	m_is_only_body(false),
	m_seq(0),
	m_remain_body_length(0)
	{

	}
	Packet::~Packet()
	{
		m_head_map.clear();
	}
	const string& Packet::GetCmd()
	{
		return m_cmd;
	}
	bool Packet::IsRequest()
	{
		return m_is_request;
	}
	void Packet::SetBody(const string& body)
	{
		m_body = body;
	}

	void Packet::SetOnlyBodyFlag()
	{
		m_is_only_body = true;;
	}

	void Packet::SetOnlyBody(const string& body)
	{
		m_is_only_body = true;
		m_body = body;
	}
	const string& Packet::GetBody()
	{
		return m_body;
	}
	const uint32_t& Packet::GetSeq()
	{
		return m_seq;
	}

	void Packet::SetSeq(const uint32_t& seq)
	{
		m_seq = seq;
		string seq_str = boost::lexical_cast<string>(seq);
		SetKeyValue(const_cmd_seq_key, seq_str);
	}

	bool Packet::SetKeyValue(const string& key, const string& value)
	{
		map<string, string>::iterator map_it = m_head_map.find(key);
		if (map_it != m_head_map.end())
		{
			return false;
		}
		m_head_map.insert(pair<string, string>(key, value));
		return true;
	}

	bool Packet::GetKeyValue(const string& key, string& value_ref)
	{
		map<string, string>::iterator map_it = m_head_map.find(key);
		if (map_it == m_head_map.end())
		{
			return false;
		}
		value_ref = map_it->second;
		return true;
	}
	void Packet::PrintPacket()
	{
		LOG(INFO) << "[" << m_recv_str << "]";
	}
}
