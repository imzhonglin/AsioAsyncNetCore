#include <iostream>
#include "packet/packet.h"
#include "packet/packet_factory.h"
#include "controller/controller.h"
#include "callback/callback.h"
#include "net/channel_core.h"
#include "log/log.h"
#include "thread/thread_manage.h"


using namespace weilin;
static bool glb_connected = false;
static bool glb_running = true;

void sighandler(int sig)
{
	glb_running = false;
}

void InitSignal()
{
	signal(SIGUSR1, sighandler);
	signal(SIGINT, sighandler);
}

void MainClientSocketStateHandleCb(const channel_core_shared_ptr& channel_core_sp, const SocketStatus& socket_status)
{
	std::cout << "MainSocketStateHandleCb : " << socket_status << std::endl;
	if (socket_status == SOCKET_CONNECTED)
	{
		glb_connected = true;
	}
	if (socket_status == SOCKET_COLOSED || socket_status == SOCKET_CONNECTING_TIMEOUT)
	{
		glb_connected = false;
		channel_core_sp->AsyncReConnect();
	}
}

void MainClientSendRecvResultCb(const controller_shared_ptr& controller_sp, const SendRecvStatus& send_recv_status)
{
	std::cout << controller_sp->GetSendPacket()->GetSeq() << std::endl;
	std::cout << "MainClientSendRecvResultCb#####" << send_recv_status << std::endl;

}


int main(void)
{

	std::cout << FILE_LINE_FUNCTION << std::endl;
	GoogleLog google_log("client");
	std::cout << FILE_LINE_FUNCTION  << "1111111111"<< std::endl;
	LOG(INFO) << "222222222222222222222222222222222222222";
	ThreadManage::Instance()->Init(4, 4);
	ThreadManage::Instance()->Start();

	std::cout << FILE_LINE_FUNCTION << "22222222222" << std::endl;
	call_back_shared_ptr call_back_sp = boost::make_shared<CallBack>(boost::bind(&MainClientSendRecvResultCb, _1, _2));

	call_back_sp->SetSocketStateHandleCb(boost::bind(&MainClientSocketStateHandleCb, _1, _2));
	channel_core_shared_ptr client_channel_core_sp_vect[10];
	unsigned int i = 0;
	for (i = 0; i < 10; i++)
	{
		client_channel_core_sp_vect[i] = boost::make_shared<RpcChannelCore>(CHANNEL_CMD, "192.168.7.45", 23000, 3000, call_back_sp);
		client_channel_core_sp_vect[i]->AsyncConnect();
	}

	//channel_core_shared_ptr client_channel_core_sp = boost::make_shared<RpcChannelCore>(CHANNEL_CMD, "comm", "192.168.7.45", 23000, 3000, call_back_sp);
	//client_channel_core_sp->AsyncConnect();
	sleep(4);
	int j = 0;
	while (glb_connected == true && j < 1000000)
	{
		channel_core_shared_ptr client_channel_core_sp(client_channel_core_sp_vect[j % 10]);
		j++;
		packet_shared_ptr send_sp = PacketFactory::Instance()->GetPackect(CHANNEL_CMD, PACKET_REQUEST);
		send_sp->SetKeyValue("Command", "test_hello_wode_cmd");
		send_sp->SetKeyValue("Version", "1.0.1");
		send_sp->SetKeyValue("Uid", "501");
		send_sp->SetSeq(j);
		string body = "{\"hello\":\"world\"}";
		send_sp->SetBody(body);
		controller_shared_ptr my_controller = boost::make_shared<Controller>(j, send_sp);
		client_channel_core_sp->AsyncSendController(my_controller);
		std::cout << "sleep 1s " << std::endl;
		usleep(100);
	}

//	client_epool_thread_sp->Stop(STOP_PEACE);
//	client_thread_pool_sp->Stop(STOP_PEACE);
	while (true)
	{
		std::cout << "sleep 1s " << std::endl;
		sleep(1);
	}

	return 0;
}
