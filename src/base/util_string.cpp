#include "base/util_string.h"

void CUtils::StrSplit(const string &str_src, const string& str_delim, vector<string>& vect_splited)
{
	size_t last = 0;
	size_t idx = str_src.find(str_delim, last);
	while (idx != string::npos)
	{
		vect_splited.push_back(str_src.substr(last, idx - last));
		last = idx + str_delim.length();
		idx = str_src.find(str_delim, last);
	}
	if (last < str_src.length())
	{
		vect_splited.push_back(str_src.substr(last, str_src.length() - last));
	}
}

bool is_char(int c)
{
	return c >= 0 && c <= 127;
}

bool is_ctl(int c)
{
	return (c >= 0 && c <= 31) || (c == 127);
}

bool is_tspecial(int c)
{
	switch (c)
	{
	case '(': case ')': case '<': case '>': case '@':
	case ',': case ';': case ':': case '\\': case '"':
	case '/': case '[': case ']': case '?': case '=':
	case '{': case '}': case ' ': case '\t':
		return true;
	default:
		return false;
	}
}

bool is_digit(int c)
{
	return c >= '0' && c <= '9';
}

bool is_num(const char* s)
{
	for (const char* c = s; *c; ++c)
		if (!is_digit(*c))
			return false;
	return true;
}

size_t get_delay(size_t cnt)
{
	uint32_t delay = std::min((int)cnt, 5);
	return delay * 1000;
}
