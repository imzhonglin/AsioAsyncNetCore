#include "comm/comm_struct.h"

namespace weilin
{
	void HttpTop::Reset()
	{
		m_method.clear();
		uri.clear();
		version.clear();
	};

	WaitEvent::WaitEvent()
	{
		m_is_need_wait = true;
	}

	void WaitEvent::Wait()
	{
		scope_mutex_locker scoper_locker(m_mutex);
		while (m_is_need_wait == true)
		{
			m_cond.wait(m_mutex);
		}
	}

	void WaitEvent::Signal()
	{
		scope_mutex_locker scoper_locker(m_mutex);
		m_is_need_wait = false;
		m_cond.notify_all();
	}

}
