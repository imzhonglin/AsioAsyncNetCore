#include "log/log.h"
#include <stdlib.h>
GoogleLog::GoogleLog(const char* prc_name)
{
	FLAGS_logtostderr = false;
	FLAGS_alsologtostderr = false;
	FLAGS_stderrthreshold = 3;

    FLAGS_log_prefix = true;
	FLAGS_minloglevel = google::INFO;
	FLAGS_max_log_size = 300; //3M

	FLAGS_v = 0;
	FLAGS_log_dir = "../log";
	FLAGS_logbufsecs = 20;
	FLAGS_stop_logging_if_full_disk = true;
    google::InitGoogleLogging(prc_name);
}

GoogleLog::~GoogleLog()
{
	google::ShutdownGoogleLogging();
}
