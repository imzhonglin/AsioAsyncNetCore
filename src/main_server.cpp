#include <iostream>
#include "packet/packet.h"
#include "packet/packet_factory.h"
#include "callback/callback.h"
#include "net/channel_core.h"
#include "log/log.h"
#include "thread/thread_manage.h"
#include "rpcserver/rpc_server.h"

using namespace weilin;
static bool glb_running = true;

void sighandler(int sig)
{
	glb_running = false;
}

void InitSignal()
{
	signal(SIGUSR1, sighandler);
	signal(SIGINT, sighandler);
}

void MainSendResultCb(const packet_shared_ptr& send_packet_sp, const SendStatus& send_status)
{
//	std::cout << "MainSendRecvResultCb : " << send_status << std::endl;
}
void MainServerRecvPacketCb(const channel_core_weak_ptr& channel_core_wp, const packet_shared_ptr& request_packet_sp)
{
	channel_core_shared_ptr channel_core_sp = channel_core_wp.lock();
	if (channel_core_sp == NULL)
	{
		return;
	}
	string print_out = request_packet_sp->GetBody();
	std::cout << "MainRecvPacketCbbefore : " << std::endl;
	std::cout << print_out << std::endl;
	std::cout << "MainRecvPacketCb : " << std::endl;


//	std::cout << "packet_sp->GetSeq(): " << request_packet_sp->GetSeq() << " isRequest : " << request_packet_sp->IsRequest() << std::endl;

	packet_shared_ptr response_packet_sp = PacketFactory::Instance()->GetPackect(CHANNEL_CMD, PACKET_RESPONSE);
	response_packet_sp->SetSeq(request_packet_sp->GetSeq());
	string response_body = "{\"wocao\":\"wocao\"}";
	response_packet_sp->SetBody(response_body);
	channel_core_sp->AsyncSendPacket(response_packet_sp);
}
int main(void)
{
	
	GoogleLog google_log("server");
	ThreadManage::Instance()->Init(4, 4);
	ThreadManage::Instance()->Start();

	call_back_shared_ptr call_back_sp = boost::make_shared<CallBack>(boost::bind(&MainSendResultCb, _1, _2), boost::bind(&MainServerRecvPacketCb, _1, _2));

	boost::shared_ptr<RpcServer> rpc_server_sp = boost::make_shared<RpcServer>(CHANNEL_CMD, "192.168.7.45", 23000, true, 3000, call_back_sp);
	rpc_server_sp->Init();
	rpc_server_sp->Start();
	while (true)
	{
		
		std::cout << "sleep 1s " << std::endl;
		sleep(1);
	}
	rpc_server_sp->Stop();
	sleep(3);
	std::cout << "-----------" << std::endl;
	return 0;
}
