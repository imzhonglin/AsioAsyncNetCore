#ifndef __THREAD_THREAD_POOL_H__
#define __THREAD_THREAD_POOL_H__
#include "base/macro.h"
#include "comm/comm_def.h"

namespace weilin {

	class ThreadPool : public boost::enable_shared_from_this<ThreadPool>
	{
	public:
		ThreadPool(const uint32_t& thread_num);
		~ThreadPool();
		void Init();
		void Start();
		void Stop(const StopMthod& stop_method);
		template<typename Handler>
		void Post(const Handler& handler)
		{
			m_io_service_sp->post(handler);
		}
	private:
		void Run();

		uint32_t m_thread_num;
		io_service_shared_ptr m_io_service_sp;
		io_service_work_shared_ptr m_service_work_sp;
		boost::thread_group		m_thread_group;
	};

}
#endif
