#ifndef GATE_THREAD_MANAGE_H
#define GATE_THREAD_MANAGE_H
#include "base/singleton.h"
#include "comm/comm_def.h"

#include "thread/thread_pool.h"

namespace weilin
{
	class ThreadManage : public Singleton<ThreadManage>
	{
		DISALLOW_COPY_AND_ASSIGN(ThreadManage);
	public:
		ThreadManage();
		~ThreadManage();
		bool Init(const uint32_t& epoll_tread_num, const uint32_t& thread_pool_num);
		bool Start();
		bool Stop(const StopMthod& stop_method);

	public:
		boost::asio::io_service& GetAcceptIoService();
		boost::asio::io_service& GetSocketIoService();
		template<typename Handler>
		void TreadPoolPost(const Handler& handler)
		{
			m_thread_pool_sp->Post(handler);
		}
	private:
		epoll_thread_shared_ptr m_epoll_thread_sp;
		thread_pool_shared_ptr m_thread_pool_sp;
		Status m_status;
	};
}
#endif
