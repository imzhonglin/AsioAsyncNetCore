#ifndef __THREAD_EPOLL_THREAD_H__
#define __THREAD_EPOLL_THREAD_H__
#include "base/macro.h"
#include "base/base_def.h"
#include "comm/comm_struct.h"

namespace weilin {

	class EpollThread : public boost::enable_shared_from_this<EpollThread>
{
public:
	EpollThread(const uint32_t& socket_thread_num);
	~EpollThread();
	void Init();
	void Start();
	void Stop(const StopMthod& stop_method);
	boost::asio::io_service& GetSocketIoService();
	boost::asio::io_service& GetAcceptIoService();
private:
	void Run(const io_service_shared_ptr& io_service_sp);
	
	
	io_service_shared_ptr m_accept_io_service_sp;
	io_service_work_shared_ptr m_accept_io_service_work_sp;
	uint32_t m_socket_thread_num;
	uint32_t m_index;
        vector<io_service_shared_ptr> m_socket_io_service_sp_vect;
	vector<io_service_work_shared_ptr> m_socket_io_service_work_sp_vect;
	boost::thread_group		m_thread_group;
};	

}
#endif
