#ifndef __COMM_DEF_H__
#define __COMM_DEF_H__
#include "base/base_inc.h"
#include "comm/comm_struct.h"

namespace weilin
{
//export
	class EpollThread;
	class ThreadPool;
	class TcpListener;
	class RpcChannelCore;
	class Packet;
	class Controller;
	class WaitEvent;
	class CallBack;
	class ChannelPost;
    class RpcServer;
	class CmdPacket;
	class HttpRequestPacket;
	class HttpResponsePacket;
	class FlowStatus;

	//thread
	typedef boost::shared_ptr<EpollThread> epoll_thread_shared_ptr;
	typedef boost::shared_ptr<ThreadPool> thread_pool_shared_ptr;
        //net
	typedef boost::shared_ptr<TcpListener> tcp_listener_shared_ptr;
	typedef boost::shared_ptr<RpcChannelCore> channel_core_shared_ptr;
	typedef boost::weak_ptr<RpcChannelCore> channel_core_weak_ptr;
	typedef boost::shared_ptr<ChannelPost> channel_post_shared_ptr;
	typedef boost::shared_ptr<FlowStatus> flow_status_shared_ptr;
        
        //server
        typedef boost::shared_ptr<RpcServer> rpcserver_shared_ptr;

	//event callback
	typedef boost::shared_ptr<WaitEvent> wait_event_shared_ptr;
	typedef boost::shared_ptr<CallBack> call_back_shared_ptr;

	//packet controller
	typedef boost::shared_ptr<Packet> packet_shared_ptr;
	typedef boost::shared_ptr<Controller> controller_shared_ptr;
	typedef boost::weak_ptr<Controller> controller_weak_ptr;
	typedef boost::shared_ptr<CmdPacket> cmd_packet_shared_ptr;
	typedef boost::shared_ptr<HttpRequestPacket> http_request_packet_shared_ptr;
	typedef boost::shared_ptr<HttpResponsePacket> http_response_packet_shared_ptr;

	//thread pool post func
	typedef boost::function<void(const socket_shared_ptr&)> AcceptedHandleCb;
	typedef boost::function<void(const channel_core_shared_ptr&, const SocketStatus& socket_status)> SocketStateHandleCb;

	typedef boost::function<void(const controller_shared_ptr&, const SendRecvStatus& send_recv_status)> SendRecvResultCb;
	typedef boost::function<void(const packet_shared_ptr&, const SendStatus& send_status)> SendResultCb;

	typedef boost::function<void(const channel_core_weak_ptr&, const packet_shared_ptr&)> RecvPacketCb;

}
#endif
