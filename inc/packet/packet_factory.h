#ifndef __PACKET_FACTORY_H__
#define __PACKET_FACTORY_H__
#include "base/singleton.h"
#include "comm/comm_def.h"

namespace weilin
{
	class PacketFactory : public Singleton<PacketFactory>
	{
	public:
		packet_shared_ptr GetPackect(const ChannelType& channel_type, const PacketType& packet_type);
	};
}
#endif
