#ifndef __PACKET_CMD_PACKET_H__
#define __PACKET_CMD_PACKET_H__
#include "comm/comm_def.h"

namespace weilin {
	class CmdPacket : public Packet
	{
		DISALLOW_COPY_AND_ASSIGN(CmdPacket);
	public:
		CmdPacket();
		virtual ~CmdPacket();

		bool Tobuffer(string& outBuffStr);
		ConsumerRetStatus Consume(const char& input);

	private:
		Header m_head;
		CmdConsumerStatus m_consumer_status;
	};
}

#endif
