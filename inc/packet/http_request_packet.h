#ifndef __PACKET_HTTP_REQUEST_PACKET_H__
#define __PACKET_HTTP_REQUEST_PACKET_H__
#include "comm/comm_def.h"

namespace weilin {
	class HttpRequestPacket : public Packet
	{
		DISALLOW_COPY_AND_ASSIGN(HttpRequestPacket);
	public:
		HttpRequestPacket();
		virtual ~HttpRequestPacket();
		bool Tobuffer(string& outBuffStr);
		ConsumerRetStatus Consume(const char& input);

	private:
		bool parseHttpTop(const string& top_str);
		HttpTop m_http_top;
		string m_http_top_str;
		Header m_head;
		HttpConsumerStatus m_consumer_status;
	};
}


#endif
