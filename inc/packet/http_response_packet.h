#ifndef __PACKET_HTTP_RESPONCE_PACKET_H__
#define __PACKET_HTTP_RESPONSE_PACKET_H__
#include "comm/comm_def.h"

namespace weilin {
	class HttpResponsePacket : public Packet
	{
		DISALLOW_COPY_AND_ASSIGN(HttpResponsePacket);
	public:
		HttpResponsePacket();
		virtual ~HttpResponsePacket();
		bool Tobuffer(string& outBuffStr);
		ConsumerRetStatus Consume(const char& input);

	private:
		string m_http_top_str;
		Header m_head;
	};
}


#endif
