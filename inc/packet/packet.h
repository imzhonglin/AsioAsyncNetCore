#ifndef __PACKET_PACKET_H__
#define __PACKET_PACKET_H__

#include "comm/comm_struct.h"

namespace weilin {
	class Packet : public ObjCount<Packet>
	{
		DISALLOW_COPY_AND_ASSIGN(Packet);
	public:
		Packet();
		virtual ~Packet();
		virtual bool Tobuffer(string& outBuffStr) = 0;
		virtual ConsumerRetStatus Consume(const char& input) = 0;
		const string& GetCmd();
		bool IsRequest();
		void SetBody(const string& body);
		void SetOnlyBodyFlag();
		void SetOnlyBody(const string& body);
		const string& GetBody();
		const uint32_t& GetSeq();
		void SetSeq(const uint32_t& seq);
		bool SetKeyValue(const string& key, const string& value);
		bool GetKeyValue(const string& key, string& value_ref);
		void PrintPacket();

	protected:
		bool m_is_request;
		string m_cmd;
		string m_body;
		bool m_is_only_body;
		uint32_t m_seq;
		map<string, string> m_head_map;
		string m_recv_str;
		int m_remain_body_length;
	};
}

#endif
