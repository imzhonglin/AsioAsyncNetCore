#ifndef __BASE_MACRO_H__
#define __BASE_MACRO_H__

#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(classname) \
private:                                   \
	classname(const classname &);             \
	classname &operator=(const classname &)
#endif

#ifndef FILE_LINE_FUNCTION
#define FILE_LINE_FUNCTION   __FILE__<<":" << __LINE__ << ":"<<__FUNCTION__
#else
#define FILE_LINE_FUNCTION  ""
#endif

#endif
