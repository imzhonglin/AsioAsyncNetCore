#ifndef __WEILIN_BASE_DEF_H__
#define __WEILIN_BASE_DEF_H__
#include "base/base_inc.h"

//mutex
typedef boost::mutex mutex_lock;
typedef boost::condition mutex_condition;
typedef boost::mutex::scoped_lock scope_mutex_locker;

//rw lock
typedef boost::shared_mutex rw_lock;
typedef boost::shared_lock<boost::shared_mutex> scope_read_locker;
typedef boost::unique_lock<boost::shared_mutex> scope_write_locker;

//io_service io_service_work
typedef boost::shared_ptr<boost::asio::io_service> io_service_shared_ptr;
typedef boost::shared_ptr<boost::asio::io_service::work> io_service_work_shared_ptr;
  
//socket share ptr
typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_shared_ptr;

//timer
typedef boost::shared_ptr<boost::asio::deadline_timer> timer_shared_ptr;

#endif
