#ifndef __BASE_UTIL_STRING_H__
#define __BASE_UTIL_STRING_H__
#include "base/base_inc.h" 

class CUtils
{
public:
	static void StrSplit(const string &str_src, const string& str_delim, vector<string>& set_splited);
};


bool is_char(int c);
bool is_ctl(int c);
bool is_tspecial(int c);
bool is_digit(int c);
bool is_num(const char* s);

size_t get_delay(size_t cnt);


#endif
