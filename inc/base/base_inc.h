#ifndef BASE_INC_H
#define BASE_INC_H

//system
#include <iostream>
#include <sstream>
#include <pthread.h>
#include <new>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <time.h>

using namespace std;

//boost
#include "boost/asio.hpp"
#include "boost/thread.hpp"
#include "boost/shared_ptr.hpp"
#include "boost/weak_ptr.hpp"
#include "boost/function.hpp"
#include "boost/lexical_cast.hpp" 
#include "boost/make_shared.hpp"
#include "boost/weak_ptr.hpp"
#include "boost/enable_shared_from_this.hpp"
#include "boost/thread/condition.hpp"
#include "boost/unordered_map.hpp"

#endif
