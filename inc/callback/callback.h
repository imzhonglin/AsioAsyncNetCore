#ifndef __CALLBACK_CALLBACK__H
#define __CALLBACK_CALLBACK__H
#include "comm/comm_def.h"
namespace weilin
{
	class CallBack
	{
	public:
		CallBack(const SendRecvResultCb& sendRecvResultCb);
		CallBack(const SendResultCb& sendResultCb, const RecvPacketCb& recvPacketCb);
		~CallBack();
		void SendRecvResult(const controller_shared_ptr& controller_sp,const SendRecvStatus& send_recv_status);
		void SendResult(const packet_shared_ptr& send_packet_sp, const SendStatus& send_recv_status);
		void RecvPacket(const channel_core_weak_ptr& channel_core_wp, const packet_shared_ptr& request_packet_sp);
		void SocketStateHandle(const channel_core_shared_ptr& channel_core_sp, const SocketStatus& socket_status);
		void AcceptedHandle(const socket_shared_ptr& socket_sp);
		void SetSocketStateHandleCb(const SocketStateHandleCb& socketStateHandleCb);
		void SetAcceptedHandleCb(const AcceptedHandleCb& acceptedHandleCb);
	private:

		SendRecvResultCb m_SendRecvResultCb;
		SendResultCb m_SendResultCb;
		RecvPacketCb m_RecvPacketCb;
		AcceptedHandleCb m_AcceptedHandleCb;
		SocketStateHandleCb m_SocketStateHandleCb;
	};
}

#endif
