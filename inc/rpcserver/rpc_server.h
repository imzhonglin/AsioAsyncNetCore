#ifndef __SERVER_RPC_SERVER_H__
#define __SERVER_RPC_SERVER_H__

#include "comm/comm_def.h"

namespace weilin {
	class RpcServer :public boost::enable_shared_from_this<RpcServer>
	{
		DISALLOW_COPY_AND_ASSIGN(RpcServer);
	public:
		RpcServer(const ChannelType channel_type, const string& ip, const uint16_t& port, const bool& is_keep_alive, const uint32_t& ms_timeout, const call_back_shared_ptr& call_back_sp);
		~RpcServer();
		bool Init();	
		bool Start();
		bool Stop();

	public:

		void acceptedHandleCb(const socket_shared_ptr& socket_sp);
		void socketStateHandle(const channel_core_shared_ptr& channel_core_sp, const SocketStatus& socket_status);

		void addAcceptedChannel(const uint32_t& channel_seq, const channel_core_shared_ptr& channle_core_sp);
		void removeAcceptedChannel(const uint32_t& channel_seq);

		void removeAllAcceptedChannel();

	private:
		ChannelType m_channel_type;

		string m_ip;
		uint16_t m_port;
		bool m_is_keep_alive;
		uint32_t m_ms_timeout;
		call_back_shared_ptr m_call_back_sp;
		uint32_t m_channel_seq;
		tcp_listener_shared_ptr m_tcp_listenr_sp;
		channel_core_shared_ptr m_channel_core_sp;
		boost::unordered_map<uint32_t, channel_core_shared_ptr> m_channel_unordered_map;
		mutex_lock m_mutex;
	};
}

#endif
