#ifndef __NET_CHANNEL_POST_H__
#define __NET_CHANNEL_POST_H__
#include "base/macro.h"
#include "comm/comm_def.h"

namespace weilin {
	class ChannelPost
	{
		DISALLOW_COPY_AND_ASSIGN(ChannelPost);
	public:
		ChannelPost(const call_back_shared_ptr& m_call_back_sp);
		~ChannelPost();
		//call back
		void PostRecvPacket(const channel_core_weak_ptr& channel_core_wp, const packet_shared_ptr& request_packet_sp);
		void PostSocketStatus(const channel_core_shared_ptr& channel_core_sp, const SocketStatus& socket_status);
		void PostControllerSendRecvStatus(const controller_shared_ptr& controller_sp, SendRecvStatus send_recv_status);
		void PostPacketSendStatus(const packet_shared_ptr& send_packet_sp, SendStatus send_status);

		//fuzhu function
		bool InsertController(const controller_shared_ptr& controller_sp);
		bool InsertPacket(const packet_shared_ptr& packet_sp);
		bool GetFirstSendPacket(packet_shared_ptr& packet_sp);
		void ClearAllController();
		void ClearAllPacket();
		bool FindRemovePacketBySeq(const uint32_t& seq, packet_shared_ptr& packet_sp);
		bool FindRemoveControllerBySeq(const uint32_t& seq, controller_shared_ptr& controller_sp);
	private:

		call_back_shared_ptr m_call_back_sp;
		boost::unordered_map<uint32_t, packet_shared_ptr> m_send_packet_sp_map;
		boost::unordered_map<uint32_t, controller_shared_ptr> m_controller_sp_map;
	};
}

#endif
