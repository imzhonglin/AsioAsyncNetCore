#ifndef __NET_FLOW_STATUS_H
#define __NET_FLOW_STATUS_H
#include "base/macro.h"
#include "base/base_def.h"

namespace weilin
{
	class FlowStatus
	{
		DISALLOW_COPY_AND_ASSIGN(FlowStatus);

	public:
		FlowStatus();
		~FlowStatus();
		bool IsBusy();
		void RecordSentTimeout();
		void RecordRecvTimeout();
		void ClearRecord();
	private:
		boost::unordered_map<uint32_t, uint32_t> m_send_timeout_map;
		boost::unordered_map<uint32_t, uint32_t> m_recv_timeout_map;
		uint32_t m_expire_time;
		mutex_lock m_mutex;
	};
}

#endif

