#ifndef __NET_TCP_LISTENER_H__
#define __NET_TCP_LISTENER_H__

#include "base/macro.h"
#include "comm/comm_def.h"

namespace weilin {
	class TcpListener : public boost::enable_shared_from_this<TcpListener>
{
	DISALLOW_COPY_AND_ASSIGN(TcpListener);
public:
	TcpListener(const string& ip, const uint16_t& port, const call_back_shared_ptr& call_back_sp);
    ~TcpListener();
	bool Init();	
	bool Start();
	bool Stop();
    
private:
	void startAccept();
	void acceptedHandleCb(const socket_shared_ptr& soket_sp, const boost::system::error_code& ec);
	boost::asio::ip::tcp::endpoint m_endpoint;
	boost::asio::io_service& m_io_service;
	call_back_shared_ptr m_call_back_sp;
	boost::asio::ip::tcp::acceptor m_accpetor;
	Status m_status;
};
}

#endif
