#ifndef __LOG_LOG_H__
#define __LOG_LOG_H__
#include <iostream>
#include "gflags/gflags.h"
#include "glog/logging.h"

class GoogleLog
{
public:
	GoogleLog(const char* prc_name);
	~GoogleLog();
};
#endif
