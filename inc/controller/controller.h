#ifndef __CONTROLLER_CONTROLLER_H__
#define __CONTROLLER_CONTROLLER_H__
#include "base/macro.h"
#include "comm/comm_def.h"

namespace weilin {
	class Controller
	{
		DISALLOW_COPY_AND_ASSIGN(Controller);
	public:
		Controller(const uint32_t& seq, const packet_shared_ptr& send_packet_sp);
		~Controller();
		const packet_shared_ptr& GetSendPacket();

	private:
		packet_shared_ptr m_send_packet_sp;

	public:
		timer_shared_ptr m_controller_timer_sp;
		packet_shared_ptr m_recv_packet_sp;
		channel_core_weak_ptr m_channel_core_wp;
	};
}

#endif
